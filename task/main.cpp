//
//  main.cpp
//  task
//
//  Created by 林家馨 on 9/10/15.
//  Copyright (c) 2015 jessie. All rights reserved.
//

#include <iostream>
#include "List.h"

int main(int argc, const char * argv[]) {
    mylib::List mylist = {3,4,5,6,7,8,9};
    mylist.push_front(2);
    mylist.push_back(10);
    for(mylib::Iterator myiter = mylist.begin();myiter != mylist.end();myiter.operator++())
        std::cout << myiter.Value()<<std::endl;
    mylib::Iterator lastone =mylist.end();
    std::cout<< lastone.Value()<<std::endl;
    int aa = mylist.size();
    std::cout<< aa << std::endl;
    return 0;
}
