//
//  List.h
//  task
//
//  Created by 林家馨 on 9/10/15.
//  Copyright (c) 2015 jessie. All rights reserved.
//

#ifndef __task__List__
#define __task__List__

#include <stdio.h>
#include <list>
#include <initializer_list>

using value_type = int;

namespace mylib {
    
    class Item {
    public:
        value_type value;
        Item *next;
    };
    
    class Iterator{
    public:
        Item* p;
        Iterator (Item* x):p(x){}
        Iterator& operator++(){p = p->next;return *this;}
        bool operator!=(const Iterator& rhs){return p!=rhs.p;}
        Item& operator*(){return *p;}
        value_type Value(){return p->value;}
        
    };
    
    class List {
    public:
        using size_type  = std::size_t;
        
        List( std::initializer_list<value_type> list );
        List();
       // ~List();
               
        int size();
        void front();
        void back();
        void push_back(const value_type &x);
        void push_front(const value_type &x);
        bool empty();
        Iterator begin();
        const Iterator cbegin();
        Iterator end();
        const Iterator cend();
    private:
        Item *head = nullptr;
        Item *last = nullptr;
        
        size_type _size;
    }; // END class List
    
}  // END namespace mylib


#endif /* defined(__task__List__) */

