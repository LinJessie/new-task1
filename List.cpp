//
//  List.cpp
//  task
//
//  Created by 林家馨 on 9/10/15.
//  Copyright (c) 2015 jessie. All rights reserved.
//

#include "List.h"
#include "list.h"


namespace mylib {
    
    List::List(std::initializer_list<value_type> list)
    : _size {list.size()}
    {
        for(auto a:list)
        {
            //std::cout<<"\nvalue = "<<a;
            if(head == nullptr)
            {
                Item *temp = new Item();
                temp->value = a;
                temp->next = nullptr;
                head=temp;
                last=temp;
                //next = temp;
            }
            else
            {
                Item *temp = new Item();
                temp->value = a;
                temp->next = nullptr;
                last->next = temp;
                last = temp;
            }
            
        }
    }
    
    
    List::List()
    {
    
    }
    
    bool List::empty()
    {
        if(head == nullptr)
            return true;
        
        return false;
    }
    
    int List::size ()
    {
        int sizey = 1;
        for(mylib::Iterator myiter = this->begin();myiter != this->end();myiter.operator++())
            sizey++;
        return sizey;
    }
    
    Iterator List::begin()
    {
        Iterator *begin = new Iterator(head);
        return *begin;
    }
    
    const Iterator List::cbegin()
    {
        const Iterator *begin = new Iterator(head);
        return *begin;
    }
    
    Iterator List::end()
    {
        Iterator *end = new Iterator(last);
        return *end;
    }
    
    const Iterator List::cend()
    {
        
        const Iterator *end = new Iterator(last);
        return *end;
    }
    
    void List::push_back(const value_type &x)
    {
        Item *temp = new Item();
        temp->value = x;
        last->next = temp;
        last = temp;
    }
    
    void List::push_front(const value_type &x)
    {
        Item *temp = new Item();
        temp->value = x;
        temp->next = head;
        head = temp;
    }
    
//    List::~List()
//    {
//        delete 
//        
//    }
    // END namespace mylib
}
